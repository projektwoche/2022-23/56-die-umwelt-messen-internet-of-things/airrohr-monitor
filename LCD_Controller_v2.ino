
/**
   BasicHTTPSClient.ino
    Created on: 14.10.2018
*/

#include <Arduino.h>

#include <WiFi.h>
#include <WiFiMulti.h>

#include <HTTPClient.h>

#include <LiquidCrystal_I2C.h>

#include <WiFiClientSecure.h>

#include <ArduinoJson.h>

int lcdColumns = 16;
int lcdRows = 2;
LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows);  


// This is GandiStandardSSLCA2.pem, the root Certificate Authority that signed 
// the server certifcate for the demo server https://jigsaw.w3.org in this
// example. This certificate is valid until Sep 11 23:59:59 2024 GMT
const char* rootCACertificate = \
 "-----BEGIN CERTIFICATE-----\n" \
"MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw\n" \
"TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\n" \
"cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4\n" \
"WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu\n" \
"ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY\n" \
"MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc\n" \
"h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+\n" \
"0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U\n" \
"A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW\n" \
"T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH\n" \
"B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC\n" \
"B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv\n" \
"KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn\n" \
"OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn\n" \
"jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw\n" \
"qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI\n" \
"rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV\n" \
"HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq\n" \
"hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL\n" \
"ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ\n" \
"3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK\n" \
"NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5\n" \
"ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur\n" \
"TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC\n" \
"jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc\n" \
"oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq\n" \
"4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA\n" \
"mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d\n" \
"emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=\n" \
"-----END CERTIFICATE-----\n";

// Not sure if WiFiClientSecure checks the validity date of the certificate. 
// Setting clock just to be sure...
void setClock() {
  configTime(0, 0, "pool.ntp.org");

  Serial.print(F("Waiting for NTP time sync: "));
  time_t nowSecs = time(nullptr);
  while (nowSecs < 8 * 3600 * 2) {
    delay(500);
    Serial.print(F("."));
    yield();
    nowSecs = time(nullptr);
  }

  Serial.println();
  struct tm timeinfo;
  gmtime_r(&nowSecs, &timeinfo);
  Serial.print(F("Current time: "));
  Serial.print(asctime(&timeinfo));
}


WiFiMulti WiFiMulti;

void setup() {

  Serial.begin(115200);
  // Serial.setDebugOutput(true);

 // initialize LCD
  lcd.init();
  // turn on LCD backlight                      
  lcd.backlight();

  Serial.println();
  Serial.println();
  Serial.println();

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("OpenWrt", "SPiTaGiSCuoROusticKeT");

  // wait for WiFi connection
  Serial.print("Waiting for WiFi to connect...");
  while ((WiFiMulti.run() != WL_CONNECTED)) {
    Serial.print(".");
  }
  Serial.println(" connected");

  setClock();  
}

void loop() {
  WiFiClientSecure *client = new WiFiClientSecure;
  if(client) {
    client -> setCACert(rootCACertificate);

    {
      // Add a scoping block for HTTPClient https to make sure it is destroyed before WiFiClientSecure *client is 
      HTTPClient https;
  
      Serial.print("[HTTPS] begin...\n");
      if (https.begin(*client, "https://data.sensor.community/airrohr/v1/sensor/78075/")) {  // HTTPS
        Serial.print("[HTTPS] GET...\n");
        // start connection and send HTTP header
        int httpCode = https.GET();
        lcd.init();
  
        // httpCode will be negative on error
        if (httpCode > 0) {
          // HTTP header has been send and Server response header has been handled
          Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

          // file found at server
          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            String payload = https.getString();
            Serial.println(payload);
            DynamicJsonDocument doc(3000);
            DeserializationError error = deserializeJson(doc, payload);

            if (error) {
              Serial.print("deserializeJson() failed: ");
              Serial.println(error.c_str());
              return;
            }


            double sensordatavalue_value1 = 0;
            for (JsonObject item : doc.as<JsonArray>()) {
              
            
              const char* timestamp = item["timestamp"]; // "2023-01-26 09:38:12", "2023-01-26 09:37:04", "2023-01-26 ...

              for (JsonObject sensordatavalue : item["sensordatavalues"].as<JsonArray>()) {


                const double sensordatavalue_value = sensordatavalue["value"]; // "19.8", "3.9"
                //Serial.print(sensordatavalue_value);
                Serial.println();
                const char* sensordatavalue_value_type = sensordatavalue["value_type"]; // "temperature", "humidity"
                lcd.setCursor(0, 0);
                // print static message  
                Serial.print(sensordatavalue);  
                Serial.println(); 
                if (1>sensordatavalue_value1){
                  Serial.print("hi");
                  
                }else{
                  if (sensordatavalue_value > sensordatavalue_value1){
                    lcd.setCursor(0, 1);
                    lcd.print("PM10 :"); 
                    lcd.setCursor(7, 1);
                    lcd.print(sensordatavalue_value);

                    lcd.setCursor(0, 0);
                    lcd.print("PM2.5 :"); 
                    lcd.setCursor(7, 0);
                    lcd.print(sensordatavalue_value1);
                }else{
                  lcd.setCursor(0, 1);
                  lcd.print("PM10:"); 
                  lcd.setCursor(7, 1);
                  lcd.print(sensordatavalue_value1);

                  lcd.setCursor(0, 0);
                  lcd.print("PM2.5:"); 
                  lcd.setCursor(7, 0);
                  lcd.print(sensordatavalue_value);

                }
                  }

                
                /**           
                if (sensordatavalue_value < 7.5){
                  lcd.print("PM2.5:"); 
                  lcd.setCursor(7, 0);
                  lcd.print(sensordatavalue_value);
                }
                lcd.setCursor(0, 1);
                if (sensordatavalue_value > 7.5) {
                  lcd.print("PM10 :"); 
                  lcd.setCursor(7, 1);
                  lcd.print(sensordatavalue_value);
                }
                */

              sensordatavalue_value1 = sensordatavalue_value;
                
              }
              
            }
                 
           
          }
        }else {
          Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
        }
        https.end();
      }else {
        Serial.printf("[HTTPS] Unable to connect\n");
      }

      // End extra scoping block

  Serial.println("Waiting 30s before the next round...");
  delay(30000);

    }
          HTTPClient https;
  
      Serial.print("[HTTPS] begin...\n");
      if (https.begin(*client, "https://data.sensor.community/airrohr/v1/sensor/78076/")) {  // HTTPS
        Serial.print("[HTTPS] GET...\n");
        // start connection and send HTTP header
        int httpCode = https.GET();
        lcd.init();
  
        // httpCode will be negative on error
        if (httpCode > 0) {
          // HTTP header has been send and Server response header has been handled
          Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

          
  
          // file found at server
          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            String payload = https.getString();
            Serial.println(payload);
            DynamicJsonDocument doc(3072);

            DeserializationError error = deserializeJson(doc, payload);

            if (error) {
              Serial.print("deserializeJson() failed: ");
              Serial.println(error.c_str());
              return;
            }
            
            for (JsonObject item : doc.as<JsonArray>()) {
          
              JsonArray sensordatavalu = item["sensordatavalues"];

              const char* timestamp = item["timestamp"]; // "2023-01-26 09:38:12", "2023-01-26 09:37:04", "2023-01-26 ...

              for (JsonObject sensordatavalue : item["sensordatavalues"].as<JsonArray>()) {

                const double sensordatavalue_value = sensordatavalue["value"]; // "24.8", "42.3"
                const char* sensordatavalue_value_type = sensordatavalue["value_type"]; // "temperature", "humidity"
                long long sensordatavalue_id = sensordatavalue["id"]; // 31161606500, 31161606556
                Serial.print(sensordatavalue_value);
              
                lcd.setCursor(0, 0);
                

                if (double(sensordatavalue_value) < 35){
                  lcd.print("Temperature:"); 
                  lcd.setCursor(12, 0);
                  lcd.print(sensordatavalue_value);
                }
                lcd.setCursor(0, 1);
                if (double(sensordatavalue_value) > 35) {
                  lcd.print("Humidity:"); 
                  lcd.setCursor(12, 1);
                  lcd.print(sensordatavalue_value);
              }

            }
          
          }
        } else {
          Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
        }
        https.end();
      } else {
        Serial.printf("[HTTPS] Unable to connect\n");
      }

      // End extra scoping block
    
  
    delete client;
  } else {
    Serial.println("Unable to create client");
  }}

  Serial.println();
  Serial.println("Waiting 30s before the next round...");
  delay(30000);
}